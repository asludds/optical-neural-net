import cv2
import numpy as np
import time
import pygame
import pygame.camera
import matplotlib.pyplot as plt

def main():
    cv2.namedWindow('preview')
    vc = cv2.VideoCapture(2)
    vc.set(3,1920)
    vc.set(4,1080)
    # vc.set(cv2.CAP_PROP_CONTRAST,0.1)
    # vc.set(5,30)
    if vc.isOpened():
        rval, frame = vc.read()
    else:
        rval = False

    cv2.imwrite("images/blue_image.jpg",frame)

    while rval:
        # start_time = time.time()
        cv2.imshow('preview',frame)
        rval, frame = vc.read()
        # print(time.time()-start_time)
        key = cv2.waitKey(20)
        if key == 27:
            break
    cv2.destroyWindow('preview')

# def main():
#     pygame.camera.init()
#     pygame.camera.list_cameras() #Camera detected or not
#     cam = pygame.camera.Camera("/dev/video2",(640,480))
#     cam.start()
#     img = cam.get_image()
#     imgarray = pygame.surfarray.array2d(img)
#     plt.figure()
#     plt.imshow(imgarray)
#     plt.show(block=True)

if __name__ == "__main__":
    main()
