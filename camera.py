from threading import Thread
import imutils
from picamera import PiCamera
import time
import cv2
import imageio
import numpy as np
from io import BytesIO
from PIL import Image
import pygame
import pygame.camera

class camera():
    def __init__(self):
        self.resolutionx = 1920
        self.resolutiony = 1200
        self.camera = PiCamera()
        self.camera.framerate = 35
        self.camera.awb_mode = "off"
        self.camera.awb_gains = (0.5,0.7)
        camera.resolution = (self.resolutionx,self.resolutiony)

        self.camera.led = False

        # self.camera.start_preview()


    def take_image(self):
        stream = BytesIO()
        self.camera.capture(stream, format='jpeg',use_video_port=False)
        stream.seek(0)
        returnable_image = Image.open(stream)
        # self.camera.close()
        return returnable_image

# class camera():
#     def __init__(self):
#         self.resolutionx = 1280
#         self.resolutiony = 720
#         self.vc = cv2.VideoCapture(2)
#         self.vc.set(cv2.CAP_PROP_AUTOFOCUS, 0) # turn the autofocus off
#         self.vc.set(3,self.resolutionx)
#         self.vc.set(4,self.resolutiony)
#         # self.vc.set(5,20)
#         self.capture_time = False
#         # pygame.camera.init()
#         # self.cam = pygame.camera.Camera('/dev/video2',(self.resolutionx,self.resolutiony))
#         # self.cam.start()

#     def take_image(self):
#         if(self.capture_time==True):
#             start_time = time.time()
#         self.vc.set(3,self.resolutionx)
#         self.vc.set(4,self.resolutiony)
#         rval, frame = self.vc.read()
#         if(self.capture_time==True):
#             print(time.time()-start_time)
#         if(rval == True):
#             return Image.fromarray(frame)
#         else:
#             while(rval==False):
#                 print("rval was false")
#                 time.sleep(1.0)
#                 rval,frame = self.vc.read()
#                 if(rval==True):
#                     return Image.fromarray(frame)

    # def take_image(self):
    #     time.sleep(1.0)
    #     img = self.cam.get_image()
    #     if(img==False):
    #         time.sleep(0.1)
    #         img = self.cam.get_image()
    #     imgarray = pygame.surfarray.array3d(img)
    #     return imgarray

if __name__ == "__main__":
    # pass
    start = time.time()
    camera = camera()
    # a = camera.video()
    # print(a)
    # a = camera.video()
    # print(a)
    # a = camera.video()
    # print(a)
    for i in range(40):
        camera.take_image()
        total_time = time.time()-start
        print("average time:", total_time/40.)



