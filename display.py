import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306
from PIL import Image,ImageDraw
import time
import numpy as np
import requests

class display():
    def __init__(self):
        self.width = 128
        self.height = 64
        self.number_of_pixels = self.width * self.height

    def set_leds(self,image):
        
        # Raspberry Pi pin configuration:
        RST = 24
        # 128x64 display with hardware I2C:
        disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)

        disp.begin()
        disp.clear()
        disp.display()

        clear_image = Image.new('1', (self.width, self.height))
        
        # Get drawing object to draw on image.
        draw = ImageDraw.Draw(clear_image)

        # Draw a black filled box to clear the image.
        draw.rectangle((0, 0, self.width, self.height), outline=0, fill=0)        

        # Display image.
        image = Image.fromarray(image)
        disp.image(image.convert('1'))
        disp.display()
        time.sleep(0.01)

    # def set_leds(self,image):
    #     image = image/255 
    #     address = "http://18.62.31.43:5000"
    #     iter = np.nditer(image)
    #     listy = [str(int(x)) for x in iter]
    #     a = ''.join(listy)
    #     requests.post(address + '/matrix',json={'matrix':a})


if __name__=="__main__":
    display = display()
    display.set_leds(np.ones((64,128))*255)
