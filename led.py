import time
import requests
from gpiozero import LED

# class led():
#     def __init__(self):
#         pass

#     def set_led(self,state):
#         address = "http://18.62.31.43:5000"
#         requests.post(address + '/led',json={'led':state})

class led():
    def __init__(self):
        self.led = LED(21)

    def set_led(self,state):
        if(state==0):
            self.led.off()
        elif(state==1):
            self.led.on()
        else:
            print("state was not on off it was ",state)



if __name__=="__main__":
    led = led()
    led.set_led(1)
    time.sleep(2)
    led.set_led(0)
