
'''Trains a simple convnet on the MNIST dataset.
Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

import keras
from keras.datasets import mnist
from keras.datasets import imdb
from keras.models import Sequential
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.preprocessing import sequence
from keras.layers.embeddings import Embedding
from scipy.misc import imread, imresize
from keras.models import model_from_json, load_model
import scipy as sp
import numpy as np
import pandas as pd

import random
import os
from progress.bar import Bar

"This class will allow for trained neural networks to be run on images that are sent optically"
class neural_network():
    def __init__(self,image,model_file,dataset = 'mnist'):
        self.image = image
        self.model_folder = 'nn_model/'
        self.model_file = model_file
        self.dataset = dataset + '/'

    def run_model_on_image(self,new_weights=False):
        x = self.image
        #compute a bit-wise inversion so black becomes white and vice versa
        # x = np.invert(x)
        #make it the right size
        x = sp.misc.imresize(x,(28,28))
        #convert to a 4D tensor to feed into our model
        x = x.reshape((1,28*28))
        x = x[::16,::16].reshape((1,7*7))
        x = x.astype('float32')
        x /= 255

        model = load_model(self.model_folder + self.dataset + self.model_file + '.h5')
        if(new_weights != False):
            model.set_weights(new_weights)
        out = model.predict(x)
        print(np.argmax(out))
        return np.argmax(out)

    def imdb_embedded_layer(self,imdb_example):
        model = load_model(self.model_folder + self.model_file + '.h5')
        imdb_example = sequence.pad_sequences(imdb_example, maxlen=100)
        model2 = Sequential()
        model2.add(model.layers[0])
        model2.add(model.layers[1])
        embedded_output = model2.predict(imdb_example)
        return embedded_output

    def input_output (self,model,layer_index,X):
        get_layer_output = K.function([model.layers[layer_index].input], [model.layers[layer_index+1].output])
        layer_output = get_layer_output([X])[0]
        return layer_output

    def run_model_imdb(self,imdb_example,layer2_weights = False,layer3_weights=False):
        model = load_model(self.model_folder + 'imdb/' + self.model_file + '.h5')
        imdb_example = np.reshape(imdb_example,(1,100,))
        a = model.predict(imdb_example)
        print("total prediction",a)

        wei = self.input_output(model,0,imdb_example)
        print(wei.shape,"wei")
        print(wei)

        embedded_output = self.imdb_embedded_layer(imdb_example)
        model2 = Sequential()
        # model2.add(Dense(100,activation='sigmoid',name='Hidden'))
        model2.add(model.layers[2])
        if(layer2_weights!=False):
            model2.set_weights(layer2_weights)
        else:
            model2.set_weights(model.layers[2].get_weights())


        model2_output = model2.predict(embedded_output)
        
        model3 = Sequential()
        model3.add(model.layers[3])
        # model3.add(Dense(1, activation='sigmoid',name="Output"))
        if(layer3_weights!=False):
            model3.set_weights(layer3_weights)
        else:
            model3.set_weights(model.layers[3].get_weights())

        model3_output = model3.predict(model2_output)
        print("step by step model output",model3_output)

        # sample = np.reshape(imdb[100],(1,100,))
        
        return [1 if x > 0.5 else 0 for x in model3_output]


    def batch_predict_mnist(self,to_predict,shape=(28,28),new_weights=False):
        model = load_model(self.model_folder + 'mnist/' + self.model_file + '.h5')
        if(new_weights != False):
            model.set_weights(new_weights)
        correct_count = 0
        bar = Bar("Testing MNIST on ONN",max=len(to_predict))
        for index,i in enumerate(to_predict):
            mnist_image , numberOfImage = load_mnist_image(number=i)
            x = sp.misc.imresize(mnist_image,(shape[0],shape[1]))

            x = x.reshape((1,shape[0]*shape[1]))
            x = x.astype('float32')
            x /= 255

            out = model.predict(x)
            prediction = np.argmax(out)
            if(prediction == i):
                correct_count += 1
            bar.next()
        bar.finish()
        return correct_count

    def batch_predict_imdb(self,new_weights):
        model = load_model(self.model_folder + self.model_file + ".h5")
        top_words = 200
        (X_train, y_train), (X_test, y_test) = imdb.load_data(num_words=top_words)
        max_words = 100
        X_train = sequence.pad_sequences(X_train, maxlen=max_words)
        print(new_weights)
        for i in new_weights:
            print(i.shape)

        prediction = model.predict(X_train)
        equ = np.equal([1 if x > 0.5 else 0 for x in prediction], y_train)
        return np.sum(equ)

    def load_weights_from_model(self):
        model = load_model(self.model_folder + self.dataset + self.model_file + '.h5')
        weights = model.get_weights()
        return weights

    def load_weights_from_model_8bit(self):
        model = load_model(self.model_folder + self.dataset + self.model_file + '.h5')
        weights = model.get_weights()
        weightlist8bit = []
        for i in weights:
            weightlist8bit.append(i.astype("float16"))
        return weightlist8bit

    def train_imdb(self):
        top_words = 200
        (X_train, y_train), (X_test, y_test) = imdb.load_data(num_words=top_words)
        max_words = 100
        X_train = sequence.pad_sequences(X_train, maxlen=max_words)
        X_test = sequence.pad_sequences(X_test, maxlen=max_words)

        model = Sequential()
        model.add(Embedding(input_dim = top_words, output_dim = 2, input_length=max_words, name="Embedding"))
        model.add(Flatten())
        model.add(Dense(100,activation='sigmoid',name='Hidden'))
        model.add(Dense(1, activation='sigmoid',name="Output"))
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        print(model.summary())
        # Fit the model
        model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=5, batch_size=128)
        # Final evaluation of the model
        score = model.evaluate(X_test, y_test, verbose=0)

        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
        model.save(self.model_folder + "imdb/" + self.model_file + '.h5')



    def train_mnist(self):
        batch_size = 128
        num_classes = 10
        epochs = 30

        # input image dimensions
        img_rows, img_cols = 7, 7

        # the data, shuffled and split between train and test sets
        (x_train, y_train), (x_test, y_test) = mnist.load_data()

        def resize_xs(xs):
            N,dim = xs.shape
            if dim != 28*28:
                print('size wrong', dim)
                return
            xs_new = np.zeros((N, img_rows*img_cols),dtype='float32')
            for i in range(N):
                xi = xs[i].reshape(28,28)
                temp = sp.misc.imresize(xi,size=(img_rows,img_cols),interp='cubic')
                xs_new[i] = temp.reshape(img_rows*img_cols)
            return xs_new

        x_train = x_train.reshape(60000, 784)
        x_test = x_test.reshape(10000, 784)
        x_train = resize_xs(x_train)
        x_test = resize_xs(x_test)

        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255
        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')

        # convert class vectors to binary class matrices
        y_train = keras.utils.to_categorical(y_train, num_classes)
        y_test = keras.utils.to_categorical(y_test, num_classes)

        model = Sequential()
        model.add(Dense(30, activation='relu', input_shape=(img_rows*img_cols,)))
        model.add(Dense(num_classes, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer='adam',
                    metrics=['accuracy'])

        model.fit(x_train, y_train,
                batch_size=batch_size,
                epochs=epochs,
                verbose=1,
                validation_data=(x_test, y_test))
        score = model.evaluate(x_test, y_test, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
        model.save(self.model_folder + 'mnist/' + self.model_file + '.h5')

def load_mnist_image(number=9):
    choice = random.choice(os.listdir("mnist_images/training/" + str(number)))
    image = sp.misc.imread("mnist_images/training/" + str(number) + "/" + choice , mode='L')
    return image,number

def load_imdb_data():
    top_words = 200
    (X_train, y_train), (X_test, y_test) = imdb.load_data(num_words=top_words)
    max_words = 100
    X_train = sequence.pad_sequences(X_train, maxlen=max_words)
    return X_train, y_train

if __name__ == "__main__":
    # pass
    image,number = load_mnist_image(number=6)
    neuralNetwork = neural_network(image=image,model_file="model")

    # neuralNetwork.train_imdb()
    # weights = neuralNetwork.load_weights_from_model()
    # for i in weights:
    #     print(i.shape)
    # imdb_data, y_data = load_imdb_data()
    # neuralNetwork.run_model_imdb(imdb_data)
    
    neuralNetwork.train_mnist()
    neuralNetwork.run_model_on_image()
    to_predict = [int(i/(1000/10)) for i in range(1000)]
    weights = neuralNetwork.load_weights_from_model_8bit()
    correct_count = neuralNetwork.batch_predict_mnist(shape=(7,7),to_predict=to_predict,new_weights=weights)
    print("Accuracy: ", correct_count/1000 * 100, "%")

