#!/usr/bin/env python
from PIL import Image, ImageDraw, ImageFont
import cv2
from io import BytesIO
import numpy as np
import scipy as sp
from skimage import feature
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
from skimage import measure
from skimage.color import rgb2gray
from math import sqrt
import time
from led import led
import sys
import struct
import pickle
from camera import camera 
from display import display
from progress.bar import Bar
from neural_net import load_mnist_image
from neural_net import load_imdb_data
from neural_net import neural_network
import imageio
from scipy import ndimage

plt.rcParams['figure.figsize'] = [20, 10]

import os

display = display()

camera = camera()

led = led()

class grid():
    def __init__(self):
        self.number_of_dots_in_x_axis = 32
        self.number_of_dots_in_y_axis = 32
        self.top_left = np.array([681,430])
        self.bottom_left = np.array([690,720])
        self.bottom_right = np.array([1260,706])
        self.top_right = np.array([1253,414])
        self.pixel_threshold = 130
        self.red_local_x = 1540
        self.red_local_y = 750
        self.led_threshold = 100
        self.area_radius = 2
        self.print_led_value = False
        self.grid = self.generate_grid(top_left=self.top_left,top_right=self.top_right,bottom_left=self.bottom_left,bottom_right=self.bottom_right)


    '''
    Runs a test that shows if the grid is alligned. If the grid is not alligned the program terminates and the file grid.png can be viewed to see the allignment.
    '''
    def test_grid_allignment(self,shape=(32,32)):
        led.set_led(0)
        image_utils_class = image_utils()
        sample_array = image_utils_class.generate_sample_array_binary()
        #update grid coordinates to be determined based on initial_grid_creation()
        measured_array_off, led_value_off = image_utils_class.extract_matrix_from_binary_array(dot_shape = shape, detect_led = True ,binary_array= sample_array,savefig=True,update_grid=True)
        if(np.array_equal(measured_array_off,np.ones(shape)) and led_value_off==0):
            print("The grid is alligned, led off. Led has been detected.")
        elif(np.array_equal(measured_array_off,np.ones(shape)) and led_value_off==1):
            print("led was misdiagnosed")
            sys.exit()
        elif(led_value_off==0):
            print("The grid is not alligned")
            print("Alligned:",np.sum(measured_array_off==np.ones(shape)))
            sys.exit()
        else:
            print("Grid and led are not alligned")
            sys.exit()
        print("on")
        led.set_led(1)
        measured_array_on, led_value_on = image_utils_class.extract_matrix_from_binary_array(dot_shape = shape, detect_led = True ,binary_array= sample_array,savefig=True)
        if(np.array_equal(measured_array_on,np.ones(shape))):
            print("The grid is allignment, led on")
        else:
            print("Led on grid not alligned")
            print("Alligned:",np.sum(measured_array_on==np.ones(shape)))
            sys.exit()
        if(led_value_on==1):
            print("Led on correctly identified. Led detected.")
        else:
            print("Led on not correctly identified")
            sys.exit()

        #Attempting a random test
        print("Random")
        random_array = image_utils_class.generate_sample_array_binary(random=True)
        random_array_on, led_value_on = image_utils_class.extract_matrix_from_binary_array(dot_shape = shape, detect_led = True, binary_array = random_array, savefig = True)
        random_array = random_array[::2,::4]
        print("random-array-shape",random_array.shape)
        if(np.array_equal(random_array,random_array_on)):
            print("Random grid is alligned")
        else:
            print("Random grid not alligned")
            print("Alligned:",np.sum(random_array_on==random_array))
            difference_array = np.abs(np.subtract(random_array_on,random_array))
            try:
                sp.misc.imsave("images/difference_array_random.png",difference_array)
            except:
                pass
            sys.exit()

        if(led_value_on==1):
            print("Random led value is correct")
            return
        else:
            print("Random led value is incorrect")
            sys.exit()

    '''
    Given the function arguments it crafts a grid which is used to find pixel values
    '''
    def generate_grid(self,top_left,bottom_left,top_right,bottom_right):
        dots_x = self.number_of_dots_in_x_axis
        dots_y = self.number_of_dots_in_y_axis
        dot_coordinates = np.zeros((dots_x,dots_y,2),dtype=object)
        for i in range(dots_x):
            for j in range(dots_y):
                p1 = top_left + i/(dots_x-1) * (top_right-top_left)
                p2 = bottom_left + i/(dots_x-1)  * (bottom_right-bottom_left)
                pt = p1 + j/(dots_y-1) * (p2-p1)
                dot_coordinates[i,j,0] = pt[0]
                dot_coordinates[i,j,1] = pt[1]
        return dot_coordinates

    def order_points(self,pts):
	    # initialzie a list of coordinates that will be ordered
	    # such that the first entry in the list is the top-left,
	    # the second entry is the top-right, the third is the
	    # bottom-right, and the fourth is the bottom-left
	    rect = np.zeros((4, 2), dtype = "float32")
        
	    # the top-left point will have the smallest sum, whereas
	    # the bottom-right point will have the largest sum
	    s = pts.sum(axis = 1)
	    rect[0] = pts[np.argmin(s)]
	    rect[2] = pts[np.argmax(s)]
    
	    # now, compute the difference between the points, the
	    # top-right point will have the smallest difference,
	    # whereas the bottom-left will have the largest difference
	    diff = np.diff(pts, axis = 1)
	    rect[1] = pts[np.argmin(diff)]
	    rect[3] = pts[np.argmax(diff)]
    
	    # return the ordered coordinates
	    return rect

    def four_point_transform(self,image, pts):
	    # obtain a consistent order of the points and unpack them
        # individually
        rect = self.order_points(pts)
        (tl, tr, br, bl) = rect

        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        # compute the height of the new image, which will be the
	    # maximum distance between the top-right and bottom-right
	    # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        # now that we have the dimensions of the new image, construct
	    # the set of destination points to obtain a "birds eye view",
	    # (i.e. top-down view) of the image, again specifying points
	    # in the top-left, top-right, bottom-right, and bottom-left
	    # order
        dst = np.array([
		    [0, 0],
		    [maxWidth - 1, 0],
		    [maxWidth - 1, maxHeight - 1],
		    [0, maxHeight - 1]], dtype = "float32")

        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

        # return the warped image
        return warped

    '''
    Given the image and the grid it maps the grid onto the image and returns a numpy array representing the matrix of values on the screen.
    '''
    def get_binary_values_from_image_with_grid(self,image,grid):
        returnable = np.zeros((grid.shape[0],grid.shape[1]))
        # for x in range(grid.shape[0]):
            # for y in range(grid.shape[1]):
                # grid_coordinate = grid[y,x]
        for x in range(grid.shape[1]):
            for y in range(grid.shape[0]):
                grid_coordinate = grid[y,x]
                area_rad = self.area_radius
                pixel_value = np.max(image[int(round(grid_coordinate[0]))-area_rad:int(round(grid_coordinate[0]))+area_rad+1,int(round(grid_coordinate[1]))-area_rad:int(round(grid_coordinate[1]))+area_rad+1])
                if(pixel_value > self.pixel_threshold):
                    returnable[y,x] = 1
                else:
                    returnable[y,x] = 0
        return returnable

    def crop_blue(self,image):
        upleft = self.top_left
        downleft = self.bottom_left
        downright = self.bottom_right
        upright = self.top_right

        points = [upleft,downleft,downright,upright]
        points = np.array(points)

        blue_image_cropped = self.four_point_transform(image,points)
        return blue_image_cropped


    def initial_grid_creation(self,image,savefig):

        # image = np.array(image)

        returnable = np.zeros(self.grid.shape)

        blue_image_cropped = image

        #First we will filter the image according to the threshold
        image_thresh_upper = blue_image_cropped > self.pixel_threshold
        blue_image_cropped[image_thresh_upper] = 255

        image_thresh_lower = blue_image_cropped <= self.pixel_threshold
        blue_image_cropped[image_thresh_lower] = 0

        Image.fromarray(blue_image_cropped).save("images/cropped_and_threshold.png")

        blobs = blue_image_cropped == 255

        labels, nlabels = ndimage.label(blobs)

        r, c = np.vstack(ndimage.center_of_mass(blue_image_cropped, labels, np.arange(nlabels) + 1)).T

        coordinates = np.zeros((r.shape[0],2))
        coordinates[:,0] = r
        coordinates[:,1] = c
        #put r,c into numpy array of shape (1024,2)

        coordinates = self.sort_2d(coordinates)
        
        fig, ax = plt.subplots()
        plt.scatter(r,c)
        for x in range(coordinates.shape[0]):
            for y in range(coordinates.shape[1]):
                value = coordinates[x,y]
                ax.annotate((y,x),(value[0],value[1]))
        plt.savefig("images/grid2.png")

        if(savefig==True):
            image = Image.fromarray(image)
            image.save("images/blue_image_cropped.png")
            draw = ImageDraw.Draw(image)
            for line in coordinates:
                for point in line:
                    y,x = point
                    draw.point((x,y))
            image.save("images/grid.png")

        if(nlabels != self.number_of_dots_in_x_axis * self.number_of_dots_in_y_axis):
            print("When updating grid within initial_grid_creation only found ",nlabels," rather than", self.grid.shape[0]*self.grid.shape[1])

            sys.exit()

        self.grid = coordinates

    def sort_2d(self,coordinates):
        returnable = np.zeros_like(self.grid)
        #find bottom 32 points in coordinates
        count = 0
        for i in range(32):
            a = coordinates[:,1].argsort()[:32]
            coors = coordinates[a]
            #sort coors along x axis
            coors = np.sort(coors,axis=0)
            coordinates[a,1] = np.inf
            returnable[:,i,:] = coors
            
        return returnable


gridclass = grid()


class image_utils():
    def importImageFromFile(self,filename):
        image = Image.open(filename)
        return image

    def crop_image(self,image, crop_coordinates):
        cropped_image = image.crop(crop_coordinates)
        return cropped_image

    def return_color_channel(self,image,channel):
        red, green, blue =  image.split()
        if(channel == "red"):
            return red
        elif(channel == "blue"):
            return blue
        elif(channel == "green"):
            return green
        else:
            print("Error, channel is not red green or blue")

    '''
    Generates a grid of dots which is 32x32 with zeros inbetween for testing the grid alignment
    '''
    def generate_sample_array_binary(self,random=False):
        sample_array = np.zeros((display.height,display.width),dtype=np.uint8)
        for y in range(sample_array.shape[0]):
            for x in range(sample_array.shape[1]):
                #The display is 128 by 64, so if we want 32x32 we need to step through 4 at a time on the x axis etc
                if(x%4==0 and y%2==0 and random==False):
                    sample_array[y,x] = 1
                if(x%4==0 and y%2==0 and random==True):
                    sample_array[y,x] = np.random.randint(2)
        return sample_array

    '''
    This method takes an array and then runs it into a binary uint8 array that is of size display.height x display.width
    '''
    def fill_area_with_zeros(self,new_shape,array):
        returnable = np.zeros((display.height,display.width),dtype=np.uint8)
        y_delta = int(display.height/new_shape[0])
        x_delta = int(display.width/new_shape[1])
        for x in range(display.width):
            for y in range(display.height):
                if(x%x_delta==0 and y%y_delta==0):
                    if(array[int(y * new_shape[0]/display.height),int(x * new_shape[1]/display.width)]== 1):
                        returnable[y,x] = 255
        return returnable

    '''
    Takes a numpy array representing an image and creates a new array by padding the original with zeros until it is 32x32
    '''
    def pad_image_to_32by32(self,image):
        cimage = image.copy()
        cimage.resize((32,32))
        return cimage

    def extract_matrix_from_binary_array(self,dot_shape,binary_array,detect_led=False,savefig=False,grid=False,update_grid=False):
        testArray = self.fill_area_with_zeros(new_shape=binary_array.shape,array=binary_array)
        display.set_leds(testArray)
        image_string = camera.take_image()
        if(detect_led==True):
            led_boolean = self.detect_led_presence(image_string,save_image=savefig)
        #get a color channel
        blue_image = self.return_color_channel(image_string,'blue')
        if(savefig==True):
            blue_image.save("images/blue_image.png")

        blue_image = np.array(blue_image)

        blue_image = gridclass.crop_blue(blue_image)

        if(savefig==True):
            blue_image_c = Image.fromarray(blue_image)
            blue_image_c.save("images/blue_image_cropped.png")
        if(update_grid==True):
            gridclass.initial_grid_creation(blue_image,savefig)

        

        #We are going to get the binary values from the image that correspond to the grid
        binary_grid_values = gridclass.get_binary_values_from_image_with_grid(blue_image,gridclass.grid)

        #transpose this to compersate for it not being the right dimensions
        # binary_grid_values = binary_grid_values.T

        if(detect_led==False):
            return np.flipud(np.fliplr(binary_grid_values))
        else:
            return np.flipud(np.fliplr(binary_grid_values)),led_boolean

    def detect_led_presence(self,image_string,save_image=False):
        red_image = self.return_color_channel(image_string,'red')
        # red_image = image_string[:,:,0].T
        if(save_image==True):
            red_image.save("images/red_led_image.png",'PNG')
        red_image_array = np.array(red_image)
        value = red_image_array[gridclass.red_local_y,gridclass.red_local_x]
        if(gridclass.print_led_value):
            print(value)
        if(value > gridclass.led_threshold):
            return 1
        else:
            return 0

image_utils_class = image_utils()


class communication_handler():
    def __init__(self,image=np.zeros((2,2))):
        self.image = image
        self.height = self.image.shape[0]
        self.width = self.image.shape[1]

    def better_binary_repr(self,pixel_value,num_bits=8):
        #if the value that is being passed in is basically a int then pass the binary of the int (this is a string)
        if(num_bits==16):
            return bin(np.float16(pixel_value).view('H'))[2:].zfill(16)
        if(int(pixel_value)-pixel_value == 0):
            return np.binary_repr(pixel_value.astype(int),width=num_bits)
        else:
            #if we have a value which is meant to be floats
            return ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', pixel_value))

    def better_pack_bits(self,topack):
        if(len(topack)==8):
            return np.packbits(topack.astype(int))
        if(len(topack)==16):
            topackstring = ''.join([str(int(x)) for x in topack])
            a=struct.pack("H",int(topackstring,2))
            return np.frombuffer(a, dtype = np.float16)[0]
        else:
            string_topack = ''.join(str(int(x)) for x in topack)
            f = int(string_topack, 2)
            packed = struct.unpack('f', struct.pack('I', f))[0]
            return packed

    def binarystringtoarray(self,binarystring):
        returnable = []
        for i in binarystring:
            returnable.append(int(i))
        return np.array(returnable)

    def decode_image(self,num_bits=8):
        #We are going to create a list of arrays which we will pipe into through the system one at a time
        decoded_image = np.zeros((self.height,self.width,num_bits))
        for x in range(self.width):
            for y in range(self.height):
                binary_representation = self.better_binary_repr(self.image[y,x],num_bits=num_bits)
                binary_representation = self.binarystringtoarray(binary_representation)
                decoded_image[y,x] = binary_representation
        return decoded_image

    def encode_image(self,array):
        returnable = np.zeros((array.shape[0],array.shape[1]))
        for x in range(array.shape[1]):
            for y in range(array.shape[0]):
                value = self.better_pack_bits(array[y,x,:])
                returnable[y,x] = value
        return returnable

    def send_images(self,num_bits=8):
        decoded_image = self.decode_image(num_bits=num_bits)
        returned_matrix_storage = np.zeros((self.height,self.width,num_bits))
        grid = gridclass.grid
        for i in range(num_bits):
            returned_matrix = image_utils_class.extract_matrix_from_binary_array(dot_shape = (self.height,self.width) ,binary_array = decoded_image[:,:,i],grid=grid) 
            returned_matrix_storage[:,:,i] += returned_matrix
        return self.encode_image(returned_matrix_storage)

    def send_images_with_led(self,led_value,num_bits=8):
        #convert led value in num_bits of binary
        binary_led = self.better_binary_repr(led_value,num_bits)
        binary_led = self.binarystringtoarray(binary_led)
        decoded_image = self.decode_image(num_bits=num_bits)
        returned_matrix_storage = np.zeros((self.height,self.width,num_bits))
        if(type(led_value)==np.uint8):
            return_led = np.zeros(8)
        elif(type(led_value)==np.float64):
            return_led = np.zeros(num_bits)
        else:
            print("Binary led value is not zero or one")
        grid = gridclass.grid
        for i in range(num_bits):
            if(binary_led[i]==0):
                led.set_led(0)
            elif(binary_led[i]==1):
                led.set_led(1)
            else:
                print("binary led value is not zero or one")
            returned_matrix, sent_led = image_utils_class.extract_matrix_from_binary_array(dot_shape = (self.height,self.width), detect_led=True, binary_array = decoded_image[:,:,i],grid=grid)
            #check for value of led
            if(type(led_value)==np.uint8):
                if(i>=24):
                    return_led[i-24] = sent_led
            elif(type(led_value)==np.float64):
                return_led[i] = sent_led
            else:
                print("sent led was not of type int or float")
            if(binary_led[i]!=sent_led):
                print("Binary_led did not match. Value to send was:",binary_led[i], " got ", sent_led)
            returned_matrix_storage[:,:,i] += returned_matrix

        if(len(return_led)==8):
            return_led_value = np.packbits(return_led.astype(int))[0]
        elif(len(return_led)==32):
            string_topack = ''.join(str(int(x)) for x in return_led)
            f = int(string_topack,2)
            packed = struct.unpack('f',struct.pack('I',f))[0]
            return_led_value = packed
        return self.encode_image(returned_matrix_storage) , return_led_value

    def filter_image(self,image,filter_size):
        cimage = image.copy()
        flattened_image = cimage.flatten()
        #return image[:filter_shape[0],:filter_shape[1]]
        return flattened_image[:filter_size]

    def result_checker(self,originalarray,finalarray,tolerance=False):
        if(np.array_equal(originalarray,finalarray)):
            return True
            # print("The original and sent matricies are equal") 
        elif(tolerance != False and np.allclose(originalarray,finalarray,atol=tolerance)):
            return True
        else:
            number_of_true = np.sum(np.isclose(originalarray,finalarray,atol=tolerance))
            total_number = originalarray.size
            accuracy = float(number_of_true * 100.0 / total_number)
            print("The accuracy is : " + str(accuracy),'%')
            difference_array = np.abs(np.subtract(finalarray,originalarray))
            try:
                sp.misc.imsave("images/difference_array.png",difference_array)
            except:
                pass
            return False

class transfer_data():
    '''
    Takes an image, pads it to a 32x32 image and sends it. Returns the transfered image
    '''
    def transfer_image(self,image,check_results=True):
        padded_weight_image = image_utils_class.pad_image_to_32by32(image)
        communication = communication_handler(padded_weight_image)
        sent_image = communication.send_images(num_bits=8).astype(int)
        if(check_results):
            communication.result_checker(padded_weight_image,sent_image)
        return sent_image

    '''
    Takes weights from the neural network and transfers them
    '''
    def transfer_weight(self,weights):
        if(weights.shape[0]==784):
            layer_object = np.zeros((784,10))
        elif(weights.shape[0]==10):
            layer_object = np.zeros((10,10))
        else:
            print("layer object not created")
        bar = Bar("Transfering layer weight",max=weights.shape[1])
        for weight in range(weights.shape[1]):
            if(weights[:,weight].shape[0]==784):
                resized_layer = np.resize(weights[:,weight],(28,28))
                filter_shape = (28,28)
            elif(weights[:,weight].shape[0]==10):
                resized_layer = np.resize(weights[:,weight],(10,1))
                filter_shape = (10,1)
            else:
                print("Layer size is incorrect")
            padded_weight_image = image_utils_class.pad_image_to_32by32(resized_layer)
            communicationweight = communication_handler(padded_weight_image)
            sent_weight = communicationweight.send_images(num_bits=32)
            communicationweight.result_checker(padded_weight_image,sent_weight)
            filter_sent_weight = communicationweight.filter_image(sent_weight,filter_shape)
            layer_object[:,weight] = np.reshape(filter_sent_weight, (weights.shape[0]))
            bar.next()
        bar.finish()
        return layer_object

    '''
    Takes biases from the neural network and transfers them
    '''
    def transfer_biases(self,biases):
        bar = Bar("Transfering layer biases", max=1)
        resized_layer = np.resize(biases,(len(biases),1))
        filter_shape = (len(biases),1)
        padded_weight_image = image_utils_class.pad_image_to_32by32(resized_layer)
        communicationweight = communication_handler(padded_weight_image)
        sent_weight = communicationweight.send_images(num_bits=16)
        communicationweight.result_checker(padded_weight_image,sent_weight)
        filter_sent_weight = communicationweight.filter_image(sent_weight,filter_shape)
        layer_object = np.reshape(filter_sent_weight, (filter_sent_weight.shape[0],))
        bar.next()
        bar.finish()
        return layer_object

    def convert_to_float32(self,inputnumber):
        return ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', inputnumber))

    '''
    Inputs:
    weights : numpy array of size layer[n].size x layer[n+1].size. For example, for first mnist layer this is 784 x 10
    image : 
    Outputs:
    The sent weights and images
    '''
    def transfer_weight_with_led_being_image(self,weights,image,progress_bar = True):
        flattened_image = image.flatten()
        returnable_weights = np.zeros(weights.shape)
        returnable_image = np.zeros(flattened_image.shape)
        if progress_bar == True: bar = Bar("Transfering pixel and weight", max=len(flattened_image))
        for pixelindex, pixel in enumerate(flattened_image):
            weightarray = weights[pixelindex]
            #weightarray = self.weightList_to_weightarray(weightList)
            padded_weight_image = image_utils_class.pad_image_to_32by32(weightarray)
            communicationweight = communication_handler(padded_weight_image)
            #sent_weight,sent_led = communicationweight.send_images_with_led(led_value = pixel ,num_bits=16)
            sent_weight,sent_led =  communicationweight.send_images_with_led(led_value = pixel , num_bits=32)
            returnable_image[pixelindex] = sent_led
            communicationweight.result_checker(padded_weight_image,sent_weight,tolerance=0.01)
            filter_sent_weight = communicationweight.filter_image(weightarray,weightarray.shape[0])
            returnable_weights[pixelindex] += np.reshape(filter_sent_weight,(filter_sent_weight.size,))
            if progress_bar == True: bar.next()
        if progress_bar == True: bar.finish()
        return returnable_weights,returnable_image

    def weightList_to_weightarray(self,weightList):
        return weightList
        return np.reshape(weightList,(weightList.shape[0],1))

    def apply_activation_function_to_sent_weight_and_image(self,sent_weight,sent_image,biases,activation_function = "relu"):
        next_layer = np.zeros(biases.shape[0])
        for pixelindex,pixel in enumerate(sent_image):
            #pixel * w + b 
            next_layer = np.add(pixel * sent_weight[pixelindex,:] + biases,next_layer)
        if(activation_function=="relu"):
            return next_layer * (next_layer > 0)
        elif(activation_function=="sigmoid"):
            return 1./(1 + np.exp(next_layer))
        elif(activation_function=="softmax"):
            e_x = np.exp(next_layer - np.max(next_layer))
            return e_x / e_x.sum(axis=0)
        else:
            print("Activation function is not available")

    def transfer_and_run_neural_network_layer(self,image,weights,bias=np.zeros(2),activation_function='relu',return_weights=False,progress_bar = True):
        sent_weights, sent_flattened_image = self.transfer_weight_with_led_being_image(weights,image,progress_bar = progress_bar)
        if(np.allclose(sent_weights,weights,atol=1e-3)):
            pass
        else:
            print("Sent weights incorrect")
            print("Original weights ",weights)
            print("Sent weights ",sent_weights)
        if(np.allclose(image.flatten(),sent_flattened_image,atol=1e-3)):
            pass
        else:
            print("Sent flattened image not equal")
            print(iamge.flatten())
            print(sent_flattened_image)
        layer_output = self.apply_activation_function_to_sent_weight_and_image(sent_weight=sent_weights,sent_image=sent_flattened_image,biases=bias,activation_function=activation_function)
        if(return_weights==False):
            return layer_output
        elif(return_weights==True):
            return layer_output, sent_weights

transfer = transfer_data()

def test_accuracy_of_transfered_weights(number_to_test_on=1000): 
    with open('sent_weights/sent.txt','rb') as weight_file:
        weights = pickle.load(weight_file)
        #itterate over 10,000 range mnist images
        numbers_to_predict = [int(i/(number_to_test_on/10)) for i in range(number_to_test_on)]
        neuralNetwork = neural_network(image=np.zeros((2,2)),model_file="model")
        # number_correct_guessed = neuralNetwork.batch_predict(shape=(14,14),to_predict=numbers_to_predict,new_weights=weights)
        number_correct_guessed = neuralNetwork.batch_predict_imdb(new_weights = weights)
        print("Accuracy is :", number_correct_guessed/number_to_test_on *100, '%')

def send_mnist(number=3,progress_bar = True,print_weights = True):
    mnist_image, numberOfImage = load_mnist_image(number=number)
    mnist_image = sp.misc.imresize(mnist_image,(7,7))
    neuralNetwork = neural_network(image=np.zeros((2,2)),model_file = "model")
    #weight transfer
    weightList = neuralNetwork.load_weights_from_model()
    if(print_weights == True):
        print("Print weight shapes")
        for i in weightList:
            print(i.shape)
        print("Done printing shapes")
    input_layer_weights = weightList[0]
    middle_layer_bias = weightList[1]
    middle_layer_weights = weightList[2]
    output_layer_biases = weightList[3]
    middle_layer_input = transfer.transfer_and_run_neural_network_layer(mnist_image,input_layer_weights,middle_layer_bias,'relu',progress_bar = progress_bar)
    output_layer_output = transfer.transfer_and_run_neural_network_layer(middle_layer_input,middle_layer_weights,output_layer_biases,'softmax',progress_bar = progress_bar)
    print(output_layer_output)
    print("Guess is:", np.argmax(output_layer_output))

    transferedWeightList = []

    #save the weights to a file for later testing and use
    with open('sent_weights/mnist/sent.txt','wb') as weight_file:
        pickle.dump(transferedWeightList,weight_file)

    return np.argmax(output_layer_output)

def send_imdb():
    mnist_image , numberOfImage = load_mnist_image(number=3)
    mnist_image = sp.misc.imresize(mnist_image,(7,7))
    # sent_image = transfer.transfer_image(mnist_image)
    #check if the original and new arrays are close to each other
    neuralNetwork = neural_network(image=np.zeros((2,2)),model_file="model")
    #weight transfer
    weightList = neuralNetwork.load_weights_from_model()
    print("Print weight shapes")
    for i in weightList:
        print(i.shape)
    print("Done printing shapes")

    first_dense_weights = weightList[1]
    bias_first = weightList[2]
    second_dense_weights = weightList[3]
    bias_second = weightList[4]
    x_imdb, y_imdb = load_imdb_data()
    # pass through embedding layer
    test_imdb = np.reshape(x_imdb[0],(1,100,))
    embedded = neuralNetwork.imdb_embedded_layer(test_imdb)
    hidden_layer,sent_weights1 = transfer.transfer_and_run_neural_network_layer(embedded,first_dense_weights,bias = bias_first,activation_function='sigmoid',return_weights=True)
    output, sent_weights2 = transfer.transfer_and_run_neural_network_layer(hidden_layer,second_dense_weights,bias = bias_second,activation_function='sigmoid',return_weights=True)
    print("output",output)
    print("imdb zero",x_imdb[0])

    transferedWeightList = [sent_weights1,sent_weights2]
  
    #check that the weight values that have been transfered are similar
    # print("Checking if the transfered weights are equal")
    # for i in range(len(transferedWeightList)):
        # communicationweight = communication_handler(np.zeros((2,2)))
        # communicationweight.result_checker(weightList[i],transferedWeightList[i])
    
    #save the weights to a file for later testing and use
    with open('sent_weights/imdb/sent.txt','wb') as weight_file:
        pickle.dump(transferedWeightList,weight_file)

def send_multiple_of_model(model='mnist'):
    if(model=='mnist'):
        number_sent = 0
        number_correct = 0
        while True:
            number_to_send = number_sent % 10
            guess = send_mnist(number_to_send,progress_bar = True, print_weights = False)
            number_sent += 1
            if(guess == number_to_send):
                number_correct += 1
            print("Number sent",number_to_send)
            print("Number classified:",number_sent)
            print("Number correct",number_correct)
            print("Accuracy of model so far:", number_correct/number_sent * 100)

    elif(model=='imdb'):
        number_sent = 0
        number_correct = 0
        while True:
            guess = send_imdb()

if __name__ == "__main__":
    print("Testing grid allignment")
    gridclass.test_grid_allignment()
    print("Sending image for neural network")
    send_multiple_of_model(model='mnist')
    # test_accuracy_of_transfered_weights(number_to_test_on=30000)
