import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306
from PIL import Image,ImageDraw,ImageFont
from flask import Flask, request
import time
import numpy as np
from gpiozero import LED
app = Flask(__name__)
app.debug = True


class display():
    def __init__(self):
        self.width = 128
        self.height = 64
        self.number_of_pixels = self.height * self.width

    def set_leds_over_network(self,image):
        RST = 24
        disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)

        disp.begin()
        disp.clear()
        disp.display()

        clear_image = Image.new('1',(disp.width,disp.height))

        draw = ImageDraw.Draw(clear_image)

        draw.rectangle((0,0,disp.width,disp.height), outline=0, fill=0)

        image = Image.fromarray(image)
        disp.image(image.convert('1'))
        disp.display()
        time.sleep(0.01)

class led():
    def __init__(self):
        self.led = LED(21)

    def on(self):
        self.led.on()

    def off(self):
        self.led.off()

@app.route('/matrix',methods=['POST'])
def matrix_handler():
    json = request.get_json()
    matrixstring = json['matrix']
    #cast matrixstring into a numpy array of size 128x64
    array = np.array([255*int(x) for x in matrixstring])
    arrayreshaped = np.reshape(array,(64,128))
    display.set_leds_over_network(arrayreshaped)
    return "OK"

@app.route("/led",methods=["POST"])
def led_handler():
    json = request.get_json()
    ledstatus = json['led']
    if(ledstatus==1):
        led.on()
    elif(ledstatus==0):
        led.off()
    return "OK"

@app.route("/ping",methods=['GET'])
def ping_handler():
    if(request.status_code==200):
        print("Successful ping request")
    else:
        print("Ping request failed")
    return "OK"


if __name__=="__main__":
    display = display()
    led = led()
    display.set_leds_over_network(np.ones((64,128)))
    app.run(host="0.0.0.0")
